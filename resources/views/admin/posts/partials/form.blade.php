<div class="form-group">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name', null, [
    'class' => 'form-control',
    'placeholder' => 'Ingresar nombre de post',
    ]) !!}

    <hr />
    @error('name')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, [
    'class' => 'form-control',
    'placeholder' => 'Ingresar slug de post',
    'readonly',
    ]) !!}
    <hr />
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('category_id', 'Categoria: ') !!}
    {!! Form::select('category_id', $categories, null, [
    'class' => 'form-control',
    ]) !!}
    <hr />
    @error('category_id')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <p class="font-weight-bold"></p>

    @foreach ($tags as $tag)
        <label class="mr-4">
            {!! Form::checkbox('tags[]', $tag->id, null) !!}
            {{ $tag->name }}
        </label>
    @endforeach
    <hr />
    @error('tags')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <p class="font-weight-bold"></p>

    <label class="mr-4">
        {!! Form::radio('status', 1, true) !!}
        Borrador
    </label>
    <label>
        {!! Form::radio('status', 2) !!}
        Publicado
    </label>
    <hr />
    @error('status')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="row mb-4">
    <div class="col">
        <div class="image-wrapper">
            @isset($post->image)
                <img id="picture" src="{{ Storage::url($post->image->url) }}" alt="">
            @else
                <img id="picture" src="https://cdn.pixabay.com/photo/2020/12/03/16/58/sunset-5801050_960_720.jpg" alt="">
            @endisset
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('file', 'Imagen que se mostrara en el post') !!}
            {!! Form::file('file', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ab dolorum quod labore obcaecati odio
                necessitatibus! Id tempore aliquam labore magnam officia quod, vitae, ut voluptate, veniam harum sed
                eaque?</p>
        </div>
    </div>
    <hr />
    @error('file')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="form-group">
    {!! Form::label('extract', 'Extracto: ') !!}
    {!! Form::textarea('extract', null, [
    'class' => 'form-control',
    ]) !!}
    <hr />
    @error('extract')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('body', 'Cuerpo del post: ') !!}
    {!! Form::textarea('body', null, [
    'class' => 'form-control',
    ]) !!}
    <hr />
    @error('body')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
